module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/api/register.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/api/register.ts":
/*!*******************************!*\
  !*** ./pages/api/register.ts ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongodb */ \"mongodb\");\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongodb__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var url__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! url */ \"url\");\n/* harmony import */ var url__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(url__WEBPACK_IMPORTED_MODULE_1__);\n\n // TODO: Validate phone and email already exists\n\nlet cachedDb = null;\n\nasync function connectToDatabase(uri) {\n  if (cachedDb) {\n    return cachedDb;\n  }\n\n  const client = await mongodb__WEBPACK_IMPORTED_MODULE_0__[\"MongoClient\"].connect(uri, {\n    useNewUrlParser: true,\n    useUnifiedTopology: true\n  });\n  const db = url__WEBPACK_IMPORTED_MODULE_1___default.a.parse(uri).pathname.substr(1);\n  cachedDb = client.db(db);\n  return client.db();\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (async (request, response) => {\n  const {\n    name,\n    phone,\n    email,\n    password,\n    birthday\n  } = request.body;\n  const db = await connectToDatabase(process.env.MONGODB_URI);\n  const collection = db.collection('users');\n  const alreadyEmail = await collection.findOne({\n    email: email\n  });\n\n  if (alreadyEmail) {\n    return response.status(406).json({\n      code: 406,\n      message: 'Email already used!'\n    });\n  }\n\n  const insert = await collection.insertOne({\n    name,\n    phone,\n    email,\n    password,\n    birthday,\n    createdAt: new Date(),\n    updatedAt: new Date()\n  });\n  return response.status(201).json({\n    code: 201,\n    message: 'Subscribed has success!'\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy9hcGkvcmVnaXN0ZXIudHM/YTk4MiJdLCJuYW1lcyI6WyJjYWNoZWREYiIsImNvbm5lY3RUb0RhdGFiYXNlIiwidXJpIiwiY2xpZW50IiwiTW9uZ29DbGllbnQiLCJjb25uZWN0IiwidXNlTmV3VXJsUGFyc2VyIiwidXNlVW5pZmllZFRvcG9sb2d5IiwiZGIiLCJ1cmwiLCJwYXJzZSIsInBhdGhuYW1lIiwic3Vic3RyIiwicmVxdWVzdCIsInJlc3BvbnNlIiwibmFtZSIsInBob25lIiwiZW1haWwiLCJwYXNzd29yZCIsImJpcnRoZGF5IiwiYm9keSIsInByb2Nlc3MiLCJlbnYiLCJNT05HT0RCX1VSSSIsImNvbGxlY3Rpb24iLCJhbHJlYWR5RW1haWwiLCJmaW5kT25lIiwic3RhdHVzIiwianNvbiIsImNvZGUiLCJtZXNzYWdlIiwiaW5zZXJ0IiwiaW5zZXJ0T25lIiwiY3JlYXRlZEF0IiwiRGF0ZSIsInVwZGF0ZWRBdCJdLCJtYXBwaW5ncyI6IkFBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0NBR0E7O0FBRUEsSUFBSUEsUUFBWSxHQUFHLElBQW5COztBQUVBLGVBQWVDLGlCQUFmLENBQWlDQyxHQUFqQyxFQUE4QztBQUMxQyxNQUFJRixRQUFKLEVBQWM7QUFDVixXQUFPQSxRQUFQO0FBQ0g7O0FBRUQsUUFBTUcsTUFBTSxHQUFHLE1BQU1DLG1EQUFXLENBQUNDLE9BQVosQ0FBb0JILEdBQXBCLEVBQXlCO0FBQzFDSSxtQkFBZSxFQUFFLElBRHlCO0FBRTFDQyxzQkFBa0IsRUFBRTtBQUZzQixHQUF6QixDQUFyQjtBQUtBLFFBQU1DLEVBQUUsR0FBR0MsMENBQUcsQ0FBQ0MsS0FBSixDQUFVUixHQUFWLEVBQWVTLFFBQWYsQ0FBd0JDLE1BQXhCLENBQStCLENBQS9CLENBQVg7QUFDQVosVUFBUSxHQUFHRyxNQUFNLENBQUNLLEVBQVAsQ0FBVUEsRUFBVixDQUFYO0FBQ0EsU0FBT0wsTUFBTSxDQUFDSyxFQUFQLEVBQVA7QUFDSDs7QUFFYyxzRUFBT0ssT0FBUCxFQUE0QkMsUUFBNUIsS0FBc0Q7QUFFakUsUUFBTTtBQUFFQyxRQUFGO0FBQVFDLFNBQVI7QUFBZUMsU0FBZjtBQUFzQkMsWUFBdEI7QUFBZ0NDO0FBQWhDLE1BQTZDTixPQUFPLENBQUNPLElBQTNEO0FBRUEsUUFBTVosRUFBRSxHQUFHLE1BQU1QLGlCQUFpQixDQUFDb0IsT0FBTyxDQUFDQyxHQUFSLENBQVlDLFdBQWIsQ0FBbEM7QUFDQSxRQUFNQyxVQUFVLEdBQUdoQixFQUFFLENBQUNnQixVQUFILENBQWMsT0FBZCxDQUFuQjtBQUVBLFFBQU1DLFlBQVksR0FBRyxNQUFNRCxVQUFVLENBQUNFLE9BQVgsQ0FBbUI7QUFBRVQsU0FBSyxFQUFFQTtBQUFULEdBQW5CLENBQTNCOztBQUNBLE1BQUlRLFlBQUosRUFBa0I7QUFDZCxXQUFPWCxRQUFRLENBQUNhLE1BQVQsQ0FBZ0IsR0FBaEIsRUFBcUJDLElBQXJCLENBQTBCO0FBQzdCQyxVQUFJLEVBQUUsR0FEdUI7QUFFN0JDLGFBQU8sRUFBRTtBQUZvQixLQUExQixDQUFQO0FBSUg7O0FBRUQsUUFBTUMsTUFBTSxHQUFHLE1BQU1QLFVBQVUsQ0FBQ1EsU0FBWCxDQUFxQjtBQUN0Q2pCLFFBRHNDO0FBRXRDQyxTQUZzQztBQUd0Q0MsU0FIc0M7QUFJdENDLFlBSnNDO0FBS3RDQyxZQUxzQztBQU10Q2MsYUFBUyxFQUFFLElBQUlDLElBQUosRUFOMkI7QUFPdENDLGFBQVMsRUFBRSxJQUFJRCxJQUFKO0FBUDJCLEdBQXJCLENBQXJCO0FBVUEsU0FBT3BCLFFBQVEsQ0FBQ2EsTUFBVCxDQUFnQixHQUFoQixFQUFxQkMsSUFBckIsQ0FBMEI7QUFDN0JDLFFBQUksRUFBRSxHQUR1QjtBQUU3QkMsV0FBTyxFQUFFO0FBRm9CLEdBQTFCLENBQVA7QUFJSCxDQTdCRCIsImZpbGUiOiIuL3BhZ2VzL2FwaS9yZWdpc3Rlci50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5vd1JlcXVlc3QsIE5vd1Jlc3BvbnNlIH0gZnJvbSBcIkB2ZXJjZWwvbm9kZVwiO1xuaW1wb3J0IHsgTW9uZ29DbGllbnQsIERiIH0gZnJvbSBcIm1vbmdvZGJcIjtcbmltcG9ydCB1cmwgZnJvbSAndXJsJztcblxuLy8gVE9ETzogVmFsaWRhdGUgcGhvbmUgYW5kIGVtYWlsIGFscmVhZHkgZXhpc3RzXG5cbmxldCBjYWNoZWREYjogRGIgPSBudWxsO1xuXG5hc3luYyBmdW5jdGlvbiBjb25uZWN0VG9EYXRhYmFzZSh1cmk6IHN0cmluZykge1xuICAgIGlmIChjYWNoZWREYikge1xuICAgICAgICByZXR1cm4gY2FjaGVkRGI7XG4gICAgfVxuXG4gICAgY29uc3QgY2xpZW50ID0gYXdhaXQgTW9uZ29DbGllbnQuY29ubmVjdCh1cmksIHtcbiAgICAgICAgdXNlTmV3VXJsUGFyc2VyOiB0cnVlLFxuICAgICAgICB1c2VVbmlmaWVkVG9wb2xvZ3k6IHRydWUsXG4gICAgfSk7XG5cbiAgICBjb25zdCBkYiA9IHVybC5wYXJzZSh1cmkpLnBhdGhuYW1lLnN1YnN0cigxKTtcbiAgICBjYWNoZWREYiA9IGNsaWVudC5kYihkYik7XG4gICAgcmV0dXJuIGNsaWVudC5kYigpO1xufVxuXG5leHBvcnQgZGVmYXVsdCBhc3luYyAocmVxdWVzdDogTm93UmVxdWVzdCwgcmVzcG9uc2U6IE5vd1Jlc3BvbnNlKSA9PiB7XG5cbiAgICBjb25zdCB7IG5hbWUsIHBob25lLCBlbWFpbCwgcGFzc3dvcmQsIGJpcnRoZGF5IH0gPSByZXF1ZXN0LmJvZHk7XG5cbiAgICBjb25zdCBkYiA9IGF3YWl0IGNvbm5lY3RUb0RhdGFiYXNlKHByb2Nlc3MuZW52Lk1PTkdPREJfVVJJKTtcbiAgICBjb25zdCBjb2xsZWN0aW9uID0gZGIuY29sbGVjdGlvbigndXNlcnMnKTtcblxuICAgIGNvbnN0IGFscmVhZHlFbWFpbCA9IGF3YWl0IGNvbGxlY3Rpb24uZmluZE9uZSh7IGVtYWlsOiBlbWFpbCB9KTtcbiAgICBpZiAoYWxyZWFkeUVtYWlsKSB7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5zdGF0dXMoNDA2KS5qc29uKHtcbiAgICAgICAgICAgIGNvZGU6IDQwNixcbiAgICAgICAgICAgIG1lc3NhZ2U6ICdFbWFpbCBhbHJlYWR5IHVzZWQhJyxcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgY29uc3QgaW5zZXJ0ID0gYXdhaXQgY29sbGVjdGlvbi5pbnNlcnRPbmUoe1xuICAgICAgICBuYW1lLFxuICAgICAgICBwaG9uZSxcbiAgICAgICAgZW1haWwsXG4gICAgICAgIHBhc3N3b3JkLFxuICAgICAgICBiaXJ0aGRheSxcbiAgICAgICAgY3JlYXRlZEF0OiBuZXcgRGF0ZSgpLFxuICAgICAgICB1cGRhdGVkQXQ6IG5ldyBEYXRlKClcbiAgICB9KTtcblxuICAgIHJldHVybiByZXNwb25zZS5zdGF0dXMoMjAxKS5qc29uKHtcbiAgICAgICAgY29kZTogMjAxLFxuICAgICAgICBtZXNzYWdlOiAnU3Vic2NyaWJlZCBoYXMgc3VjY2VzcyEnLFxuICAgIH0pO1xufSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/api/register.ts\n");

/***/ }),

/***/ "mongodb":
/*!**************************!*\
  !*** external "mongodb" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"mongodb\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJtb25nb2RiXCI/ZGVmZiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJtb25nb2RiLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibW9uZ29kYlwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///mongodb\n");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"url\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJ1cmxcIj82MWU4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6InVybC5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInVybFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///url\n");

/***/ })

/******/ });