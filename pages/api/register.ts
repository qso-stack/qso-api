import { NowRequest, NowResponse } from "@vercel/node";
import { MongoClient, Db } from "mongodb";
import url from 'url';

// TODO: Validate phone and email already exists

let cachedDb: Db = null;

async function connectToDatabase(uri: string) {
    if (cachedDb) {
        return cachedDb;
    }

    const client = await MongoClient.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    const db = url.parse(uri).pathname.substr(1);
    cachedDb = client.db(db);
    return client.db();
}

export default async (request: NowRequest, response: NowResponse) => {

    const { name, phone, email, password, birthday } = request.body;
    const db = await connectToDatabase(process.env.MONGODB_URI);
    const collection = db.collection('users');

    const alreadyEmail = await collection.findOne({ email: email });    
    if (alreadyEmail) {
        return response.status(406).json({
            code: 406,
            message: 'Email already used!',
        });
    }

    await collection.insertOne({
        name,
        phone,
        email,
        password,
        birthday,
        createdAt: new Date(),
        updatedAt: new Date()
    });

    return response.status(201).json({
        code: 201,
        message: 'Register has success!',
    });
}