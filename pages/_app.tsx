import ThemeContainer from "../contexts/theme/ThemeContainer"

function QsoApp({ Component, pageProps }) {
  return (
    <ThemeContainer>
      <Component {...pageProps} />
    </ThemeContainer>
  )
}

export default QsoApp
