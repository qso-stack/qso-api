// import Head from 'next/head'

import { Image, Flex, Button, Text, FormControl } from '@chakra-ui/core'
import { FormEvent, useState } from 'react'
import Input from '../components/Input'
import axios from "axios";

export default function Home() {

  const [ name, setName ] = useState('');
  const [ email, setEmail ] = useState('');
  const [ phone, setPhone ] = useState('');
  const [ password, setPassword ] = useState('');
  const [ confirmPass, setConfirmPass ] = useState('');
  const [ birthday, setBirthday ] = useState('');

  function handleSignUp(event: FormEvent) {
    event.preventDefault();
    axios.post('/api/register', { name, phone, email, password, birthday });
  }

  // TODO: Validate all fields

  return (
    <Flex
      as="main"
      height="100vh"
      justifyContent="center"
      alignItems="center"
      backgroundColor="gray.400"
    >
      <Flex
        as="form"
        onSubmit={handleSignUp}
        backgroundColor="gray.50"
        borderRadius="md"
        flexDir="column"
        alignItems="stretch"
        padding={8}
        marginTop={4}
        width="100%" 
        maxW="400px"
      >
        <Image marginBottom={8} src="/logo.jpg" alt="QSO GERAL" />
  
        <Text textAlign="center" fontSize="sm" color="white" marginBottom={2}>
          Preencha o cadastro para registrar-se.
        </Text>

        <Input
          placeholder="Nome completo"
          backgroundColor="gray.100"
          marginTop={ 2 }
          value={ name }
          onChange={ e => setName(e.target.value) }
        />

        <Input
          placeholder="Celular"
          backgroundColor="gray.100"
          marginTop={ 2 }
          value={ phone }
          onChange={ e => setPhone(e.target.value) }
        />
  
        <FormControl id="email">
          <Input
            placeholder="Seu melhor e-mail"
            backgroundColor="gray.100"
            marginTop={ 2 }
            value={ email }
            onChange={ e => setEmail(e.target.value) }
            type="email"
          />
        </FormControl>

        <Input
          placeholder="Senha"
          backgroundColor="gray.100"
          type="password"
          marginTop={ 2 }
          value={ password }
          onChange={ e => setPassword(e.target.value) }
        />

        <Input
          placeholder="Confirmar senha"
          backgroundColor="gray.100"
          type="password"
          marginTop={ 2 }
          value={ confirmPass }
          onChange={ e => setConfirmPass(e.target.value) }
        />

        <Input
          placeholder="Data de aniversário"
          backgroundColor="gray.100"
          marginTop={ 2 }
          value={ birthday }
          onChange={ e => setBirthday(e.target.value) }
        />
  
        <Button
          type="submit"
          backgroundColor="red.500"
          color="White"
          height="50px"
          borderRadius="sm"
          marginTop={6}
          _hover={{ backgroundColor: 'red.600' }}
        >
          REGISTRAR
        </Button>
      </Flex>
    </Flex>
  )
}
